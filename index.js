const express = require('express')
const app = express()
const port = process.env.PORT || 8080

app.get('/', (req, res) => {
  res.send("Riley's first nodejs app deployed with docker and devops. Edit 1")
  res.send("Edit made that was automatically deployed")
})

app.listen(port, '0.0.0.0', () => {
  console.log(`Example app listening at http://'0.0.0.0':${port}`)
})