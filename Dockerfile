#Set the base docker image
FROM node:12-alpine
#Set the Maintainer of the project
LABEL org.opencontainers.image.authors="cca-mirandar1@hotmail.com"

#Set the working directory
WORKDIR /usr/src/app

#Copy the packages*.json file to the working directory inside the image
COPY package*.json ./

#install nodejs packages using npm install command
RUN npm install
#Copy source code to the working directory inside the image
COPY . .
#Set the port number
EXPOSE 8080
#Indicate the command to start the container to run
CMD [ "npm", "start" ]
